import {Component, OnDestroy, OnInit} from '@angular/core';
import {Message} from "../shared/message.model";
import {MessageService} from "../shared/message.service";
import {Subscription} from "rxjs";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})
export class MessagesComponent implements OnInit , OnDestroy{

  messages!: Message[];
  messagesSubscription!: Subscription;

  apiUrl = environment.apiUrl;

  constructor(private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.messagesSubscription = this.messageService.messagesChange.subscribe((messages: Message[]) => {
      this.messages = messages.reverse();
    })
      this.messageService.getMessages();
  }



  ngOnDestroy() {
    this.messagesSubscription.unsubscribe();
  }
}
