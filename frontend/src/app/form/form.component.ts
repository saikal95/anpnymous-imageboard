import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {messageData} from "../shared/message.model";
import {MessageService} from "../shared/message.service";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {

@ViewChild('f')messageForm! : NgForm;

  constructor( private messageService: MessageService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const messageData : messageData  = this.messageForm.value;
    const next = () => {
      this.messageService.getMessages()
    };

    this.messageService.postMessage(messageData).subscribe(next);

  }
}
