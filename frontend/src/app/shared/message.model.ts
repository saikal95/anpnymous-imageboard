export class Message{
  constructor(
    public _id: string,
    public message: string,
    public author: string,
    public datetime: string,
    public image: string,
  ){}
}



export interface messageData {
  [key: string] :any,
  message: string,
  author: string,
  image: File | null;
}
