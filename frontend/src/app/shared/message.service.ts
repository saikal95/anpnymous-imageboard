import { Injectable } from '@angular/core';
import {Message, messageData} from "./message.model";
import {map, Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  messages: Message[] | null | undefined;
  messagesChange = new Subject <Message[]>();


  constructor(private http: HttpClient) {}

  getMessages() {
    return this.http.get<Message[]>(environment.apiUrl + '/messages').pipe(
      map(response => {
        return response.map(messageData => {
          return new Message(
            messageData._id,
            messageData.message,
            messageData.author,
            messageData.datetime,
            messageData.image
          );
        });
      })
    ).subscribe( {
    next: result => {
      this.messages = result;
      this.messagesChange.next(this.messages.slice());
    }, error: err => (
        console.log(' THe message is empty')
      )
  })
  }

    postMessage(messageData: messageData) {
      const formData = new FormData();
      Object.keys(messageData).forEach(key => {

        if(messageData[key] !== null){
          formData.append(key, messageData[key]);
        }
      });
      return this.http.post(environment.apiUrl +'/messages', formData);
    }


}
