const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../fileDb');
const router = express.Router();


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file,cb)=> {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', (req, res) => {
  const messages = db.getItems();
  return res.send(messages);

});


router.post('/', upload.single('image'), async (req, res, next) => {

  try {

    const post = {
      author: req.body.author,
      message: req.body.message,
    }

    if (req.file) {
      post.image = req.file.filename;
    }

    await db.addItem(post);

    return res.send({id: post.id, author: post.author, message: post.message, datetime: post.datetime});

  } catch (e) {
    next(e);
  }


});

module.exports = router;